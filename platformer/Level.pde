class Level
{
  PImage fire;
  PImage leaves;
  PImage sky;
  PImage spikes;
  PImage coin;
 
  Level() 
  {
    lcheck();
    fire = loadImage("fire.jpeg");
    leaves = loadImage("leaves.jpg");
    sky = loadImage("sky.jpg");
    spikes = loadImage("spikes.png");
    coin = loadImage("coin.png");
    
    imageMode(CENTER);
  }

  void backgrounds() // any background images
  {
    if (levels == 1)
    {
      image(fire, width/2, height/2, width, height);
      fill(255);
      text(score, 10, 10);
      text((millis() - time)/1000, 100, 10);      
    }
    if (levels == 2)
    {
      image(leaves, width/2, height/2, width, height);
      fill(255);
      text(score, 10, 10);
    }
    if (levels == 3)
    {
      image(sky, width/2, height/2, width, height);
      fill(255);
      text(score, 10, 10);
    }
    if (levels == 0)
    {
      menu();
    }
    if (levels == 4)
    {
      finalScore();
    }
  }

  void menu()// press r to get to level 1
  {
    cheats = true;
    background(0);
    
    textSize(32);
    text("A++", 280, 50);
    textSize(16);
    text("Collect the points", 50, 300);
    
    text("Watch out for spikes and collapsing blocks", 300, 300);
    
    level[27][4] = 1;
    level[24][7] = 1;
    level[22][9] = 1;
    points[21][6] = 1;
    
    spike[25][29] = 1;
    collapse[28][35] = 1;
    
    text("Press r to begin", 260, 150);
    
  }

  void levelOne()
  {
    cheats = false;
    time = millis(); // resets time
    tlimit = 15; // time limit in secounds
    maxpoints = 15; // maxpoints need to progress
       
      startX = 0;
      startY = 27;
      p1.x = 0;
      p1.y = 27*16;
    
    for(int i = 28; i < 30; i++)
    {
      for(int j = 0; j < 40; j++)
      {
        level[i][j] = 1;
      }
    }
    spike[27][5] = 1;
    points[25][5] = 1;
    
    for(int i = 22; i < 24; i++)
    {
      for(int j = 0; j < 30; j++)
      {
        level[i][j] = 1;
      }
    }
    spike[25][9] = 1;
    points[26][12] = 1;
    spike[27][22] = 1;
    points[25][18] = 1;
    
    spike[27][35] = 1;
    level[26][37] = 1;
    points[26][39] = 1;
    level[23][39] = 1;
    level[22][34] = 1;
    level[19][32] = 1;
    
    for(int i = 14; i < 16; i++)
    {
      for(int j = 10; j < 40; j++)
      {
        level[i][j] = 1;
      }
    }
    
    points[18][30] = 1;
    spike[21][26] = 1;
    level[19][23] = 1;
    points[17][21] = 1;
    
    level[18][20] = 1;
    
    for (int j = 17; j < 23; j = j + 2)
    {
      spike[21][j] = 1;
    }
    
    level[19][17] = 1;
    points[20][14] = 1;
    collapse[20][1] = 1;
    level[19][0] = 1;
    points[17][4] = 1;
    level[16][5] = 1;
    collapse[18][4] = 1;
    collapse[14][7] = 1;
    level[15][8] = 1;
    level[13][4] = 1;
    points[11][3] = 1;
    collapse[9][0] = 1;
    level[10][1] = 1;
    
    for(int i = 6; i < 8; i++)
    {
      for(int j = 0; j < 30; j ++)
      {
        level[i][j] = 1;
      }
    }
    
    spike[13][11] = 1;
    collapse[11][11] = 1;
    level[13][12] = 1;
    points[13][14] = 1;
    spike[9][17] = 1;
    level[12][19] = 1;
    spike[12][21] = 1;
    points[9][22] = 1;
    level[10][23] = 1;
    collapse[11][24] = 1;
   
    for(int j = 27; j < 39; j = j + 2)
    {
      spike[13][j] = 1;
    }
    
    level[11][27] = 1;
    collapse[10][29] = 1;
    level[10][30] = 1;
    level[11][33] = 1;
    level[10][35] = 1;
    level[8][38] = 1;
    collapse[7][36] = 1;
    level[6][35] = 1;
    level[5][31] = 1;
    points[10][34] = 1;
    points[4][38] = 1;
    
    for(int i = 0; i < 2; i++)
    {
      for(int j = 10; j < 40; j++)
      {
        level[i][j] = 1;
      }
    }
    
    spike[5][26] = 1;
    points[4][24] = 1;
    spike[3][20] = 1;
    points[3][17] = 1;
    spike[3][15] = 1;
    spike[5][8] = 1;
  
  }
  void levelTwo()
  {
    startX = 39;
    startY = 1;
    p1.x = 39*16;
    p1.y = 1*16;
    
    cheats = false;
    maxpoints = 10;
    
    level[27][36] = 1;
    points[14][37] = 1;
    level[27][33] = 1;
    level[27][30] = 1;
    level[27][27] = 1;
    points[26][28] = 1;
    
    level[24][29] = 1;
    collapse[23][28] = 1;
    level[22][26] = 1;
    points[18][27] = 1;
    level[19][29] = 1;
    collapse[16][30] = 1;
    level[17][30] = 1;
    level[14][26] = 1;
    level[11][28] = 1;
    level[8][29] = 1;
    points[7][28] = 1;
    level[7][25] = 1;
    points[5][22] = 1;
    
    for(int i = 3; i < 23; i = i + 2)
    {
      spike[i][19] = 1;
    }
    
    level[27][22] = 1;
    level[27][19] = 1;
    collapse[26][14] = 1;
    level[27][16] = 1;
    level[27][13] = 1;
    points[25][18] = 1;
    
    level[24][11] = 1;
    collapse[21][15] = 1;
    level[20][14] = 1;
    level[22][10] = 1;
    points[19][12] = 1;
    spike[17][15] = 1;
    
    collapse[18][13] = 1;
    level[17][11] = 1;
    level[14][10] = 1;
    level[12][13] = 1;
    level[9][14] = 1;
    collapse[6][12] = 1;
    level[7][11] = 1;
    points[6][14] = 1;
    
    points[6][5] = 1;
    points[25][6] = 1;
    
    for(int i = 0; i < 25; i++)
    {
      for(int j = 0; j < 2; j++)
      {
        level[i][j] = 1;
      }
    }
    
    for(int j = 3; j < 16; j = j + 2)
    {
      spike[1][j] = 1;
    }
    
    for(int i = 8; i < 30; i++)
    {
      for(int j = 8; j < 10; j++)
      {
        level[i][j] = 1;
      }
    }
    
    for(int i = 0; i < 23; i++)
    {
      for(int j = 16; j < 18; j++)
      {
        level[i][j] = 1;
      }
    }
    
    for(int j = 11; j < 24; j = j + 2)
    {
      spike[29][j] = 1;
    }
    
    for(int i = 8; i < 30; i++)
    {
      for(int j = 24; j < 26; j++)
      {
        level[i][j] = 1;
      }
    }
    
    for(int j = 19; j < 31; j = j + 2)
    {
      spike[1][j] = 1;
    }
    
    for(int i = 0; i < 23; i++)
    {
      for(int j = 31; j < 33; j++)
      {
        level[i][j] = 1;
      }
    }
    
    for(int j = 27; j < 39; j = j + 2)
    {
      spike[29][j] = 1;
    }
    
    for(int i = 5; i < 30; i++)
    {
      for(int j = 38; j < 40; j++)
      {
        level[i][j] = 1;
      }
    }
  }

  void levelThree()
  {
    cheats = false;
    maxpoints = 8;
    
    startX = 0;
    startY = 28;
    p1.x = 0;
    p1.y = 28*16;
    
    for(int j = 3; j < 40; j = j + 2)
    {
      spike[29][j] = 1;
    }
    
    level[29][0] = 1;
    
   level[27][3] = 1;
   level[26][5] = 1;
   level[24][6] = 1;
   level[22][9] = 1;
   level[19][11] = 1;
   level[18][14] = 1;
   level[16][16] = 1;
   level[13][13] = 1;
   level[14][10] = 1;
   level[16][8] = 1;
   collapse[19][6] = 1;
   level[21][4] = 1;
   level[22][1] = 1;
   level[24][3] = 1;
   level[26][1] = 1;
   
   level[27-5][3+10] = 1;
   level[26-5][5+10] = 1;
   level[24-5][6+10] = 1;
   collapse[22-5][9+10] = 1;
   level[19-5][11+10] = 1;
   collapse[18-5][14+10] = 1;
   level[16- 5][16+10] = 1;
   
   level[27-11][3+10] = 1;
   collapse[26-11][5+10] = 1;
   level[24-11][6+10] = 1;
   level[22-11][9+10] = 1;
   collapse[19-11][11+10] = 1;
   level[18-11][14+10] = 1;
   level[16- 11][16+13] = 1;
   
      
   level[27-15][3+20] = 1;
   level[26-10][5+20] = 1;
   level[24-16][6+20] = 1;
   collapse[22-15][9+20] = 1;
   level[19-15][11+20] = 1;
   level[18-14][14+20] = 1;
   level[16- 13][16+20] = 1;
   level[10][29] = 1;
   
   level[2][39] = 1;
   
   points[24][5] = 1;
   points[21][9] = 1;
   points[16][11] = 1;
   points[19][3] = 1;
   points[1][39] = 1;
   points[20][15] = 1;
   points[14][18] = 1;
   points[8][23] = 1;
  }

  void finalScore()// press r on final to go back to menu
  {
    cheats = true;
    background(0);
    textSize(32);
    text(scores[0], width/2, 50); // highscore
    textSize(20);
    for (int i = 1; i < scores.length; i++) // all attempts
    {
      text(scores[i], width/2, 100 + i * 50);
    }
  }

  void update()
  {
    fill(255);
    noStroke();
    for ( int ix = 0; ix < WIDTH; ix++ )
    {
      for ( int iy = 0; iy < HEIGHT; iy++ )
      {
        if (level[iy][ix] == 1) 
        {
          if(levels == 1)
          {
            fill(209, 0, 0);
          }
          if(levels == 2)
          {
            fill(62, 32, 11);
          }
          if(levels == 3)
          {
            fill(255);
          }
          rect(ix*16, iy*16, 16, 16);
        }
        if (points[iy][ix] == 1) 
        {
          image(coin, ix*16, iy*16, 12, 12);
        }
        if (collapse[iy][ix] == 1) 
        {
          if(levels == 1)
          {
            fill(77, 0, 0);
          }
          if(levels == 2)
          {
            fill(183, 145, 109);
          }
          if(levels == 3)
          {
            fill(180, 255, 249);
          }
          rect(ix*16, iy*16, 16, 16);
        }
        if (spike[iy][ix] == 1) 
        {
          // rotate(100);
          image(spikes, ix*16, iy* 16, 18, 18);
        }
      }
    }
  }

  boolean placeFree(int xx, int yy) 
  {
    //checks if location is free
    yy = int(floor(yy/16.0));
    xx = int(floor(xx/16.0));

    boolean answer = false;

    if ( xx > -1 && xx < level[0].length && yy > -1 
      && yy < level.length ) 
    {
      if ( level[yy][xx] == 0 ) 
      {
        answer = true;
      }
      if ( points[yy][xx] == 1 ) 
      {
        answer = true;
        points[yy][xx] = 0;
        score = score + 5;
        collected = collected+ 1;
        if ( collected >= maxpoints)
        {
          ldone = true;
          lreset();
          levels = levels + 1;
          lcheck();
          timer();
        }
      }
      if ( collapse[yy][xx] == 1 ) 
      {
        collapse[yy][xx] = 0;
      }
      if ( spike[yy][xx] == 1 ) 
      {
        p1.death();
      }
    }

    return answer;
  }

  void cheats(boolean enabled)
  {
    if (enabled)
    {
      if (level[int(mouseY/16.0)][int(mouseX/16.0)] == 0)
      {
        level[int(mouseY/16.0)][int(mouseX/16.0)] = 1;
      } else 
      {
        level[int(mouseY/16.0)][int(mouseX/16.0)] = 0;
      }
    }
  }

  void lreset()
  {
    for ( int ix = 0; ix < WIDTH; ix++ )
    {
      for ( int iy = 0; iy < HEIGHT; iy++ )
      {
        if (level[iy][ix] == 1) 
        {
          level[iy][ix] = 0;
        }
        if (points[iy][ix] == 1) 
        {
          points[iy][ix] = 0;
        }
        if (collapse[iy][ix] == 1) 
        {
          collapse[iy][ix] = 0;
        }
        if (spike[iy][ix] == 1) 
        {
          spike[iy][ix] = 0;
        }
      }
    }
  }

  void lcheck() // checks level and resets collected
  { 
    collected = 0;

    if (levels == 1)
    {
      levelOne();
    } 
    if (levels == 2)
    {
      levelTwo();
    } 
    if (levels == 3)
    {
      levelThree();
    } 
    if (levels == 0)
    {
      menu();
    } 
    if (levels == 4)
    {
      finalScore();
    }
    ldone = false;
  }

  // to use but time = millis at start of level and set tlimit
  void timer()
  {
    if ((millis() - time)/1000 <= tlimit && ldone)
    {
      score= score + 100;
      time = millis();
    }
  }
}

